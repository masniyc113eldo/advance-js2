const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootElement = document.getElementById("root");
const ulElement = document.createElement("ul");

class PropertyMissingError extends Error {
    constructor(property) {
        this.name = 'PropertyMissingError';
    }
}

class Book {
    constructor(title, name, price) {
        this.title = title;
        this.name = name;
        this.price = price;
    }

    validate() {
        if (!this.title) {
            throw new PropertyMissingError('title');
        }
        if (!this.name) {
            throw new PropertyMissingError('name');
        }
        if (!this.price) {
            throw new PropertyMissingError('price');
        }
    }
}

books.forEach(bookData => {
    try {
        const book = new Book(bookData.title, bookData.name, bookData.price);
        book.validate();
        const liElement = document.createElement("li");
        liElement.innerText = `${book.name} - ${book.title}, ${book.price} грн`;
        ulElement.appendChild(liElement);

    }  catch (err) {
        if (err.name === "CatchBookError") {
            console.warn(err);
        } else {
            throw err;
        }
    }
});